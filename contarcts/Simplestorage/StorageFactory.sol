//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import './SimpleStorage.sol';
contract StorageFacory{
    SimpleStorage[] public simpleStorageArr;
    function createSimpleStorage() public {
        simpleStorageArr.push(new SimpleStorage());
    }
    function sfTore(uint256 _simpleStorageIndex,uint256 _simpleStorageNumber) public {
        simpleStorageArr[_simpleStorageIndex].store(_simpleStorageNumber);
    }
    function sfRetrieve(uint256 _simpleStorageIndex)public view  returns(uint256){
        return simpleStorageArr[_simpleStorageIndex].retrieve();
    }
}