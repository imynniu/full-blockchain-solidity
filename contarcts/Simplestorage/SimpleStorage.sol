//SPDX-License-Identifier: MIT
pragma solidity ^0.8.7; //指定特定的版本
contract SimpleStorage{
    uint  favoriteNumber ;
    mapping(string => uint256) public nameToNumber;
    struct People {
	    uint256 favoriteNumber;
	    string name;
    }
    People[] public people;
    function addPerson(string memory _name,uint _favoriteNumber)public {
        people.push(People(_favoriteNumber,_name));
        nameToNumber[_name] = _favoriteNumber;
    }

    function store(uint _favoriteNumber) public virtual{
        favoriteNumber=_favoriteNumber;
    }
    //retrive,相当于getter
    function retrieve() public view returns(uint256){
        return favoriteNumber;
    } 





}
