//Get funds from users
//withdraw funds
//Set a minimum funding value in USD
//	897421>872528
//SPDX-License-Identifier: MIT
pragma  solidity ^0.8.0;
import './PriceConverter.sol';
error notOwner();
contract FundMe{
    using PriceConverter for uint256;
    uint256 constant MINIMUM_USD = 50 * 1e18;
    address public immutable i_owner ;
    //0xD4a33860578De61DBAbDc8BFdb98FD742fA7028e
    address[] public funders;
    //immutable 	422gas
    //non immutable  2555gas
    mapping(address => uint256) public addressToAmountFunded;
    constructor(){
        i_owner = msg.sender;
    }

    function fund() public payable{
        require(msg.value.getConvertionRate() > MINIMUM_USD,"Don't send enough");
        funders.push(msg.sender);
        addressToAmountFunded[msg.sender] = msg.value;

    }
    function withdraw() public  onlyOwner{
        for(uint256 index = 0;index<funders.length;index++){
           address funderAddress = funders[index];
           addressToAmountFunded[funderAddress] = 0;
        }
        funders = new address[](0);
        (bool callSuccess,) = payable(msg.sender).call{value : address(this).balance}("");
        require(callSuccess,"call failed");
    }
    modifier onlyOwner{
        // require(msg.sender == i_owner,"you are not the contract owner");
        //817085  - 846093 差距挺大的
        if(msg.sender != i_owner){revert notOwner();}
        _;
    }
    receive()external payable{
        fund();
    }
    fallback()external payable{
        fund();
    }
}