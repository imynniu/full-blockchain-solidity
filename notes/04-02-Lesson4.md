---
title: solidity中的特殊函数
date: 2023.4.2
categories: 
- solidity
tags:
- solidity 
---

## 特殊函数

[特殊函数](https://docs.soliditylang.org/en/latest/contracts.html#special-functions)

### 构造函数

- 在合约部署的时候运行一次
- 初始化一些状态变量

### receive

1. 执行的时机

   - 直接向合约地址转账时触发

2. 介绍地址

   - 没有function关键字
   - 不能有返回值
   - 必须要有external的visibility
   - 必须是payable的
   - It can be virtual, can override and can have modifiers.
   - 一个合约中只有一个receive函数

3. 示例

   ```solidity
       receive()external payable{
           fund();
       }
   ```

   

### fallback

1. 执行的时机
   - 调用合约中的不存在的函数时
   - 直接向一个合约发送以太，但是receive()函数不存在，或者msg.data不为空的时候

2. [介绍地址](https://solidity-by-example.org/fallback/)
   - 没有function关键字
   - 一个合约最多有一个fallback函数
   - 必须要有external 的visibility
   - A **fallback** function can be virtual, can override and can have modifiers.

3. 示例

   ```solidity
       fallback()external payable{
           fund();
       }
   ```

   