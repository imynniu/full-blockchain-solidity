---
title: 节省GAS
date: 2023.4.1
categories: 
- solidity
tags:
- solidity 
---

### Constant & Immutable

这两个标识符标识的变量不会保存在storage内，所以更加省gas

#### constant

- 表明变量不会发生改变
- 后面不能更改标识的变量
- 更加省gas
- 标识后一般全大写

#### Immutable

- 标识之后标识变量是可变的
- 同样可以省gas
- 通常命名方式`i_name`
- 只可以在构造函数中更改一次

### 定制错误

require语句中的消息，也是需要占存储的，因此可以使用定制化的错误提示，不使用require语句，也可以更加的省gas

1. 声明方式

   在合约外标注

   ```solidity
   error notOwner();
   contract FundMe{}
   ```

2. 替换require语句

   ```solidity
   modifier onlyOwner{
       // require(msg.sender == i_owner,"you are not the contract owner");
       //817085  - 846093 差距挺大的
       if(msg.sender != i_owner){revert notOwner();}
        _;
   }
   ```

   revert()可以在函数中调用

