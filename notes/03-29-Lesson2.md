---
title: 编译器版本、变量类型、函数
date: 2023.3.29
categories: 
- solidity
tags:
- solidity 
---
### SPDX开源协议的指定

- 一般放在在最开始的一行

- 是可选的

- 没有的话编译器会警告

  使用方式如下

  ```
  //SPDX-License-Identifier: MIT
  ```

### 编译器版本的指定方式

1. 指定0.8.7的编译器版本

   ```
   pragma solidity 0.8.7;
   ```

2. 指定大于0.8.7版本的编译器（包含0.8.7）

   ```solidity
   pragma solidity ^0.8.7;
   ```

3. 指定明确范围的编译器
	```solidity
	pragma solidity >=0.8.7 <0.9.0;
	```

### 合约的创建

- 使用关键字contract+contractName+{}

  ```solidity
  contract SimpleStorage{
  //合约内容
  }
  ```


### solidity的Types

##### 类型和声明变量

基本的类型

1. Boolean

   - 变量的声明

     ```solidity
     bool hasFavoriteNumber = false;
     ```

2. uint

   - 无符号整数

   - 可以指定的使用多少位uint8,通常默认的是uint256

   - 变量的声明方式，不赋值的话为0

     ```solidity
     uint favoriteNumber = 23;
     ```

     

3. int

   - 有符号整数

   - 可以指定位数，默认是int256

   - 变量的声明

     ```solidity
     int favoriteNumber = -5;
     ```

4. string 

   - 字符串

   - 可以转换为bytes类型

   - 声明方式

     ```solidity
     string favoriteNumberInText = 'five';
     ```

5. address

   - 账户的地址

   - 声明方式

     ```solidity
     address myAddress = 0x12345485148511521165264efefefrwf;
     ```

6. bytes

   - string实际上也是bytes

   - 可以指定大小，最大时bytes32，不指定时自动大小

   - 声明方式

     ```solidity
     bytes32 favoriteBytes = "cats";//0x262652626sdacd
     ```

     

### solidity Functions

1. 简介

   - 类似于其他语言的函数

   - 使用关键字`function`

     ```solidity
     //SPDX-License-Identifier: MIT
     pragma solidity 0.8.7; //指定特定的版本
     contract SimpleStorage{
         uint public favoriteNumber ;
         function store(uint _favoriteNumber) public{
             favoriteNumber=_favoriteNumber;
         }
     }
     ```

2. 部署合约

   1. 选定编译器进行编译
   2. Deploy

3. 函数的可见性

   - external

     只有外部的合约可以访问，当前合约内f()不生效，this.f()生效。

   - public

     可以内部访问也可以消息调用。

   - internal

     只有在本合约或者继承此合约的合约中 可以访问。

   - private

     只有在当前的合约中可以访问。

4. 变量的可见性

   - public

     编译器会自动创建一个getter方法，用于获取该值。

   - internal

   - private

5. 作用域

6. view&pure函数

   - 这两个标识符的函数不需要花费gas来运行
   - 如果在需要花费gas的函数中调用俩函数，会花费gas

   1. view
      - 不允许对区块链状态的修改
      - 只能读取状态
   2. pure
      - 不允许对区块链的状态进行修改
      - 也不能读取状态

   

