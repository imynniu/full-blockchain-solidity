---
title: 使用hardhat部署智能合约、hardhat中的网络设置、编程代码验证
date: 2023.4.3
categories: 
- solidity
tags:
- solidity 
---

### 使用hardhat部署智能合约

1. 编译智能合约

   - 命令行运行下面的命令

   ```js
   npx hardhat compile
   ```

2. 部署合约

   1. 创建ContractFactory

      - 注意这里使用的是`getContractFactory`的方法

      - 不同于ethers的直接调用的方法

        ```js
        const SimpleStorageFactory = await ethers.getContractFactory('SimpleStorage')
        ```

   2. 部署合约

      ```js
      const simpleStorage = await SimpleStorageFactory.deploy()
      ```

   3. 等待部署完成

      ```js
      await simpleStorage.deployed()
      ```

   4. 运行部署脚本

      ```js
      npx hardhat run scripts/deploy.js
      ```

3. 完整代码

   ```js
   const { ethers } = require('hardhat')
   
   const main = async () => {
     const SimpleStorageFactory = await ethers.getContractFactory('SimpleStorage')
     console.log('正在部署，请等待...')
     const simpleStorage = await SimpleStorageFactory.deploy()
     await simpleStorage.deployed()
   }
   
   main()
     .then(() => process.exit(0))
     .catch((error) => {
       console.error(error)
       process.exit(1)
     })
   
   ```

   

### hardhat中的网络设置

- hardhat有一个内置的区块链模拟网络

- akin to ganache

- 在配置文件hardhat.config.js文件中的可以配置其他网络

- 在运行部署命令的时候可以使用`--network xxx`指定要部署的网络

  ```js
  npx hardhat run scripts/deploy.js --network sepolia
  ```
  
- hardhat.config.js中的配置

  ```js
  require("@nomicfoundation/hardhat-toolbox");
  require("dotenv").config();
  /** @type import('hardhat/config').HardhatUserConfig */
  
  const PRIVATE_KEY = process.env.PRIVATE_KEY;
  const RPC_URL = process.env.RPC_URL;
  module.exports = {
    solidity: "0.8.8",
    networks: {
      sepolia: {
        url: RPC_URL,
        accounts: [PRIVATE_KEY],
        chainId: 11155111,
      },
    },
  };
  ```

  

### 编程代码验证

- hardhat中有个插件可以自动完成

1. 插件的安装

   ```js
   npm install --save-dev @nomiclabs/hardhat-etherscan
   ```

   

2. 修改配置文件

   1. 在hardhat.config.js中引入插件

      ```js
      require("@nomiclabs/hardhat-etherscan");
      ```

      

   2. 添加etherscan的apikey

      ```js
      module.exports = {
        solidity: "0.8.8",
        networks: {
          sepolia: {
            url: RPC_URL,
            accounts: [PRIVATE_KEY],
            chainId: 11155111,
          },
        },
        etherscan: {
          apiKey: ETHERSCAN_API_KEY,
        },
      };
      ```



3. 在部署的程序添加验证函数

   1. 添加验证函数

      ```js
      const verfiy = async (contractAddress, args) => {
        console.log("Verfiying contract...");
        try {
          await run("verify:verify", {
            address: contractAddress,
            constructorArguments: args,
          });
        } catch (e) {
          if (e.messsage.toLowerCase().include("has already been verified")) {
            console.log("already verified");
          } else {
            console.log(e);
          }
        }
      };
      ```

   2. 只有在真实测试网络环境下才进行验证

      ```js
        //只有当前为测试网络环境时，才进行验证
        if (network.config.chainId === 11155111 && process.env.ETHERSCAN_API_KEY) {
          await deployResponse.deployTransaction.wait(5);
          await verfiy(deployResponse.address, []);
        }
      ```

### 使用hardhat与合约进行交互

类似于直接使用ethers.js

```js
  //interact with contract
  const currentNumber = await SimpleStorage.retrieve();
  console.log(`currentNumber is : ${currentNumber}`);
  const transactionResponse = await SimpleStorage.store("62");
  await transactionResponse.wait(1);
  const updatedNumber = await SimpleStorage.retrieve();
  console.log(`updatedNumber is : ${updatedNumber}`);
```

### 完整代码

```js
const { ethers, run, network } = require("hardhat");
const main = async () => {
  const SimpleStorageFactory = await ethers.getContractFactory("SimpleStorage");
  console.log("Deploying ...");
  const SimpleStorage = await SimpleStorageFactory.deploy();
  await SimpleStorage.deployed();
  console.log("Deploy completed!");
  // console.log(network.config);

  //只有当前为测试网络环境时，才进行验证
  if (network.config.chainId === 11155111 && process.env.ETHERSCAN_API_KEY) {
    await SimpleStorage.deployTransaction.wait(5);
    await verfiy(SimpleStorage.address, []);
  }
  //interact with contract
  const currentNumber = await SimpleStorage.retrieve();
  console.log(`currentNumber is : ${currentNumber}`);
  const transactionResponse = await SimpleStorage.store("62");
  await transactionResponse.wait(1);
  const updatedNumber = await SimpleStorage.retrieve();
  console.log(`updatedNumber is : ${updatedNumber}`);
};

const verfiy = async (contractAddress, args) => {
  console.log("Verfiying contract...");
  try {
    await run("verify:verify", {
      address: contractAddress,
      constructorArguments: args,
    });
  } catch (e) {
    if (e.messsage.toLowerCase().include("has already been verified")) {
      console.log("already verified");
    } else {
      console.log(e);
    }
  }
};

main()
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });

```







   

   

