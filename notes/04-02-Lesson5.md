---
title: wsl环境配置
date: 2023.4.2
categories: 
- solidity
tags:
- solidity 
---

### wsl

- windows上的Linux子系统
- 可以像虚拟机一样直接运行

```
wsl --install
```



### 安装组件(ubuntu)

1. nvm

   ```
   curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
   ```

2. nodejs

   ```
   nvm install 16.15.2
   ```

   

3. git

- 一般自带

  ```
  git --version
  ```

  

4. 在vscode中安装solidity

   搜索插件solidity

   作者：Nomic Foundation

5. vscode中添加代码保存格式

   ```
     "[solidity]": {
       "editor.defaultFormatter": "NomicFoundation.hardhat-solidity"
     }
   ```

   - prettier :很棒的代码格式化工具
