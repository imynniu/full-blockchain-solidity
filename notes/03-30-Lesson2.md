---
title: 数组和结构体,错误和警告,Memory Storage Calldata,mapping
date: 2023.3.30
categories: 
- solidity
tags:
- solidity 
---

### array&struct

1. 结构体与数组

   - 声明方式

   ```solidity
   struct People {
   	uint256 favoriteNumber;
   	string name;
   }
   ```

   - 创建变量的方式

   ```solidity
   //方式1
   People public people = People({favoriteNumber:23,name:"jonus"});
   //方式2
   People public people = People(23,"jonus");
   ```

2. 创建数组

   - 变量类型[]+变量名称

   ```solidity
   People[] public people;//[]未指定,是动态的
   ```

   - push函数

   ```solidity
   function addPerson(string memory _name,uint _favoriteNumber)public {
      people.push(People(_favoriteNumber,_name));
   }
   ```

   

### 错误和警告

1. Yellow: warnings are ok

2. Red : Errors are not ok


### Memory Storage Calldata

- 这三个只用于标记Array,Struct,Mapping,其中string是作为array也需要标记
- Memory & calldata 标记的变量只存活在其作用域内
- memory标记的变量能修改
- calldata标记的变量不能修改
- storage 标记的变量表示持久化存储

### mapping
- 声明方式
```solidity
mapping(string => uint256) public nameToNumber;
```

- 添加方式

```sol
nameToNumber["str"] = uint;
```



