---
title: 可组合性,合约之间的交互,继承和重写
date: 2023.4.1
categories: 
- solidity
tags:
- solidity 
---

### 可组合性

- 合约之间是可以交互的，一个合约也可以部署另一个合约

1. import 关键字

   - 将合约导入到另一个合约中
   - 可以使用import关键字
   - 可以直接复制到合约内容

   ```solidity
   import './SimpleStorage.sol';
   ```

2. 创建合约变量

   ```solidity
   SimpleStorage public simpleStorage;
   ```

3. 将合约部署到区块链中,使用new关键字

   ```solidity
   simpleStorage = new SimpleStorage();
   ```

4. 完整的合约代码

   ```solidity
   //SPDX-License-Identifier: MIT
   pragma solidity ^0.8.0;
   import './SimpleStorage.sol';
   contract StorageFacory{
       SimpleStorage public simpleStorage;
       function createSimpleStorage() public {
           simpleStorage = new SimpleStorage();
       }
   }
   ```

5. 使用数组存储部署的合约

   ```solidity
   //SPDX-License-Identifier: MIT
   pragma solidity ^0.8.0;
   import './SimpleStorage.sol';
   contract StorageFacory{
       SimpleStorage[] public simpleStorage;
       function createSimpleStorage() public {
           simpleStorage.push(new SimpleStorage());
       }
   }
   ```

   

### 合约之间的交互

1. 与智能合约之间的交互总是需要两个东西

   - 合约地址
   - ABI：Application Binary Interface

2. 获取合约的字节码,生成交互的对象

   ```solidity
    //simpleStorageArr原本就是SimpleStorage类型
    SimpleStorage simpleStorage =simpleStorageArr[_simpleStorageIndex];
    //若是只有合约的地址则需要如下的方式，address
    SimpleStorage simpleStorage =SimpleStorage(address);
   ```

3. 调用目标合约中的函数

   ```solidity
   simpleStorage.store(_simpleStorageNumber);

4. 完整代码

   ```solidity
   //SPDX-License-Identifier: MIT
   pragma solidity ^0.8.0;
   import './SimpleStorage.sol';
   contract StorageFacory{
       SimpleStorage[] public simpleStorageArr;
       function createSimpleStorage() public {
           simpleStorageArr.push(new SimpleStorage());
       }
       function sfTore(uint256 _simpleStorageIndex,uint256 _simpleStorageNumber) public {
           SimpleStorage simpleStorage =simpleStorageArr[_simpleStorageIndex];
           simpleStorage.store(_simpleStorageNumber);
       }
       function sfRetrieve(uint256 _simpleStorageIndex)public view  returns(uint256){
           SimpleStorage simpleStorage =simpleStorageArr[_simpleStorageIndex];
           return simpleStorage.retrieve();
       }
   }
   ```

5. 可以调整的简化后的代码

   ```solidity
   //SPDX-License-Identifier: MIT
   pragma solidity ^0.8.0;
   import './SimpleStorage.sol';
   contract StorageFacory{
       SimpleStorage[] public simpleStorageArr;
       function createSimpleStorage() public {
           simpleStorageArr.push(new SimpleStorage());
       }
       function sfTore(uint256 _simpleStorageIndex,uint256 _simpleStorageNumber) public {
           simpleStorageArr[_simpleStorageIndex].store(_simpleStorageNumber);
       }
       function sfRetrieve(uint256 _simpleStorageIndex)public view  returns(uint256){
           return simpleStorageArr[_simpleStorageIndex].retrieve();
       }
   }
   ```

   

### 继承和重写

#### 继承：inheritance

1. 首先需要使用import关键字将目标合约导入

   ```solidity
   import './SimpleStorage.sol';
   ```

2. 使用is关键字，继承目标合约，此时当前合约将会和当前合约一致

   ```solidity
   //SPDX-License-Identifier: MIT
   pragma solidity ^0.8.0;
   import './SimpleStorage.sol';
   contract ExtraStorage is SimpleStorage{
   }
   ```

#### 重写：override

1. 在目标合约中的需要重写的函数添加`virtural`关键字

   ```solidity
       function store(uint _favoriteNumber) public virtual{
           favoriteNumber=_favoriteNumber;
       }
   ```

2. 在当前合约中的重写函数中添加`override`关键字

   ```solidity
         function store(uint _favoriteNumber) public override{
           favoriteNumber=_favoriteNumber+5;
       }
   ```

3. 完整合约代码

   ```solidity
   //SPDX-License-Identifier: MIT
   pragma solidity ^0.8.0;
   import './SimpleStorage.sol';
   contract ExtraStorage is SimpleStorage{
         function store(uint _favoriteNumber) public override{
           favoriteNumber=_favoriteNumber+5;
       }
   }
   ```

   