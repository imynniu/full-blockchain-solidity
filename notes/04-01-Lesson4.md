---
title: chainLink & Oracles
date: 2023.4.1
categories: 
- solidity
tags:
- solidity 
---

### Sending ETH Through a Function & Reverts

1. Fields in a transaction
   - Nonce 表明该交易是该账户的第几笔交易，防止双花攻击
   - Gas Price  price per uint of gas in Wei
   - Gas Limit : max gas taht tx can use
   - To address of the tx is sent to
   - Value amount of Wei to send
   - Data what send to the To-address
   - v,r,s components of tx signature

2. payable

- Functions and addresses declared `payable` can receive `ether` into the contract.

  ```solidity
      function fund() public payable{
      }
  ```

3. msg.value

   - 发送给合约的ether的数量，单位是Wei

   ```solidity
   require(msg.value > 1e18,"Don't send enough");
   ```

4. require关键字

   - require(表达式)，会对表达式中的内容进行检查，
   - 表达式为false时，会发生回滚

   ```solidity
   //参数1：表达式；参数二：错误信息        
   require(msg.value > 1e18,"Don't send enough");
   ```

5. revert

   - 比如require函数会导致revert
   - 撤销之前的操作
   - 返还剩余的gas
   - 但是，在此之前消耗的gas不会返还

### chainLink & Oracles



#### what is a blockchain oracle

- 智能合约连接问题or Oracle problem

  为了让所有的节点达成共识，blockchain在设计上时确定性的。如果增加可变数据、随机数或者api中获取的数据，每个节点所获取的数据可能不一样，这就会导致无法达成共识。

- 如果使用中心化的Oracle，又会产生新的信任问题。

- chainLink是一个去中心化的Oracle网络。

#### what is the chainLink

- chainLink是一个模块化的去中心化Oracle网络。
- 可以定制提供任何数据或者外部计算

##### chainLink的开箱即用的功能

1. chainLink Data Feeds

   通过智能合约返回token在当前的交易所中的平均价格。

2. chainlink VRF

   - 去中心化的方式获取一个链下的随机数
   - 在智能合约中可证明的获取随机数的方法
   - 保证公平性和随机性

3. chainLink Keepers

   - 去中心化的事件驱动执行
   - 可以定制一个事件触发另一个事件的监听，然后采取行动

4. any api

   - 可以去中心化的方式获取api中的内容

### importing from github and npm

```solidity
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
```



### Array and struct 2

```solidity
    address[] public funders;
    mapping(address => uint256) public addressToAmountFunded;


    function fund() public payable{
        require(getConvertionRate(msg.value) > minimumUsd,"Don't send enough");
        funders.push(msg.sender);
        addressToAmountFunded[msg.sender] = msg.value;

    }
```







